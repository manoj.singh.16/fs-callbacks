// Problem 1:

// Using callbacks and the fs module's asynchronous functions, do the following:
//     1. Create a directory of random JSON files
//     2. Delete those files simultaneously

const fs = require('fs');

const problem1 = (directory) => {
  fs.mkdir(directory, { recursive: true }, (error) => {
    if (error !== null) {
      console.log(error);
    } else {
      console.log('Directory created!');

      const fileNames = [];
      for (let i = 0; i < 5; i++) {
        const fileName = 'random' + i;

        fs.writeFile(`${directory}/${fileName}.json`, JSON.stringify({}), (error) => {
          if (error !== null) {
            console.log(error);
          } else {
            console.log(`${fileName}.json created`);
            fileNames.push(`${fileName}.json`);

            checkDelete(directory, fileNames);
          }
        });
      }
    }
  });
};

module.exports = problem1;

const checkDelete = (directory, fileNames) => {
  if (fileNames.length === 5) {
    fileNames.forEach((file) => {
      fs.unlink(`${directory}/${file}`, (error) => {
        if (error) {
          console.log(error);
        } else {
          console.log(`${file} deleted`);
        }
      });
    });
  }
};
