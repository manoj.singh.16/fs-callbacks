// Problem 2:

// Using callbacks and the fs module's asynchronous functions, do the following:
//     1. Read the given file lipsum.txt
//     2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
//     3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
//     4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
//     5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.

const fs = require('fs');

const problem2 = (filePath) => {
  fs.readFile(filePath, 'utf-8', (error, data) => {
    if (error !== null) {
      console.log(error);
    } else {
      const dataUpperCase = data.toUpperCase();
      const dataUpperCaseFile = 'dataUpperCase.txt';

      fs.writeFile(dataUpperCaseFile, dataUpperCase, (error) => {
        if (error !== null) {
          console.log(error);
        } else {
          const filenames = 'filenames.txt';

          fs.writeFile(filenames, `${dataUpperCaseFile}\n`, (error) => {
            if (error !== null) {
              console.log(error);
            } else {
              console.log(`${dataUpperCaseFile} added`);

              fs.readFile(dataUpperCaseFile, 'utf-8', (error, data) => {
                if (error !== null) {
                  console.log(error);
                } else {
                  const dataLowerCase = data.toLowerCase().split('. ').join('\n');
                  const dataLowerCaseFile = 'dataLowerCase.txt';

                  fs.writeFile(dataLowerCaseFile, dataLowerCase, (error) => {
                    if (error !== null) {
                      console.log(error);
                    } else {
                      fs.appendFile(filenames, `${dataLowerCaseFile}\n`, (error) => {
                        if (error !== null) {
                          console.log(error);
                        } else {
                          console.log(`${dataLowerCaseFile} added`);

                          fs.readFile(dataLowerCaseFile, 'utf-8', (error, data) => {
                            if (error !== null) {
                              console.log(error);
                            } else {
                              const dataSorted = data
                                .split('\n')
                                .sort((a, b) => {
                                  if (a < b) {
                                    return -1;
                                  }
                                })
                                .join('\n');

                              const dataSortedFile = 'dataSorted.txt';

                              fs.writeFile(dataSortedFile, dataSorted, (error) => {
                                if (error !== null) {
                                  console.log(error);
                                } else {
                                  fs.appendFile(filenames, dataSortedFile, (error) => {
                                    if (error !== null) {
                                      console.log(error);
                                    } else {
                                      console.log(`${dataSortedFile} added`);

                                      fs.readFile(filenames, 'utf-8', (error, data) => {
                                        if (error !== null) {
                                          console.log(error);
                                        } else {
                                          const filenamesData = data.split('\n');

                                          filenamesData.forEach((file) => {
                                            fs.unlink(file, (error) => {
                                              if (error !== null) {
                                                console.log(error);
                                              } else {
                                                console.log(`${file} removed`);
                                              }
                                            });
                                          });
                                        }
                                      });
                                    }
                                  });
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
};

module.exports = problem2;
