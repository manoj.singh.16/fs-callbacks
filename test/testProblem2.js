const problem2 = require("../problem2");

const path = require("path");

const filePath = path.join(__dirname, "../", "data/lipsum.txt");

problem2(filePath);
